# Proxy für OpenStreetMap

Karte für [asvt.ch](https://asvt.ch/).

## Ziel

Benutzerfreundliche Karte die auf asvt.ch eingebunden werden kann.

- [x] Möglichst freie Komponenten verwenden (Open Source, Creative Commons)
- [x] Datenschutzfreundlich (keine Ressourcen von Drittanbietern laden, auch nicht von OpenStreetMap)
- [x] Wartungsarm, Deploy and forget

## Aufbau

* [Leaflet](https://leafletjs.com/): JavaScript library für die darstellung der Karte
* [nginx proxy](cnf/nginx.conf): Datenschutzfreundliches einbinden von OpenStreetMap

Leaflet stellt das Framework für den Karten-Viewer bereit und rendert eingezeichnete Elemente und Markierungne. Die Kartendaten stammen von OpenStreetMap und werden über einen Nginx Proxy geladen.
Somit werden keine Daten (wie z. B. IP-Adressen) an Drittanbieter übertragen.

```
Besucher <----> Webserver <----> OpenStreetMap
```

## Links

* Demo: https://map.asvt.ch/
* Als iframe einbinden:

```html
<iframe width="400" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://map.asvt.ch/" style="border: 0px"></iframe>
```
